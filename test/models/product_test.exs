defmodule CCounter.ProductTest do
  use CCounter.ModelCase

  alias CCounter.Product

  @valid_attrs %{generic_name: "some content", ingredients: [], nutriments: %{}, product_name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Product.changeset(%Product{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Product.changeset(%Product{}, @invalid_attrs)
    refute changeset.valid?
  end
end
