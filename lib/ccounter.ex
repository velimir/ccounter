defmodule CCounter do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      # Start the endpoint when the application starts
      supervisor(CCounter.Endpoint, []),
      worker(CCounter.Repo, [config(CCounter.Repo)]),
      # Here you could define other workers and supervisors as children
      # worker(CCounter.Worker, [arg1, arg2, arg3]),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: CCounter.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    CCounter.Endpoint.config_change(changed, removed)
    :ok
  end

  def app_name do
    Dict.get(Mix.Project.config, :app)
  end

  def config(key) do
    Application.fetch_env!(app_name, key)
  end

  def config(key, subkey) do
    config(key) |> Dict.fetch!(subkey)
  end
end
