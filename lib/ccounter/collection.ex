defmodule CCounter.Collection do
  defmacro __using__(opts) do
    collection = Keyword.fetch!(opts, :name)
    pool       = Keyword.fetch!(opts, :pool)

    quote do
      @collection unquote(collection)
      @pool       unquote(pool)

      # TODO: replace with a macros
      def aggregate(pipeline, opts \\ []) do
        Mongo.aggregate(@pool, @collection, pipeline, opts)
      end

      def count(filter, opts \\ []) do
        Mongo.count(@pool, @collection, filter, opts)
      end

      def distinct(field, filter, opts \\ []) do
        Mongo.distinct(@pool, @collection, field, filter, opts)
      end

      def find(filter, options \\ []) do
        Mongo.find(@pool, @collection, filter, options)
      end

      def insert_one(document, options \\ []) do
        Mongo.insert_one(@pool, @collection, document, options)
      end

      def insert_many(documents, options \\ []) do
        Mongo.insert_many(@pool, @collection, documents, options)
      end

      def delete_one(filter, options \\ []) do
        Mongo.delete_one(@pool, @collection, filter, options)
      end

      def delete_many(filter, options \\ []) do
        Mongo.delete_many(@pool, @collection, filter, options)
      end

      def replace_one(filter, replacement, opts \\ []) do
        Mongo.replace_one(@pool, @collection, filter, replacement, opts)
      end

      def update_one(filter, update, opts \\ []) do
        Mongo.update_one(@pool, @collection, filter, update, opts)
      end

      def update_many(filter, update, opts \\ []) do
        Mongo.update_many(@pool, @collection, filter, update, opts)
      end

      def save_one(doc, opts \\ []) do
        Mongo.save_one(@pool, @collection, doc, opts)
      end

      def save_many(docs, opts \\ []) do
        Mongo.save_many(@pool, @collection, docs, opts)
      end
    end
  end
end
