defmodule CCounter.Repo do
  use Mongo.Pool, name: __MODULE__, adapter: Mongo.Pool.Poolboy

  def run_command(query, opts \\ []) do
    Mongo.run_command(__MODULE__, query, opts)
  end
end
