use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :ccounter, CCounter.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :ccounter, CCounter.Repo,
  database: "off-fr",
  pool_size: 10,
  max_overflow: 10,
  timeout: 10000
