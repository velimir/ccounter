// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "deps/phoenix_html/web/static/js/phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"

var bloodhound_names = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        url: '/api/products?product_name=%QUERY',
        wildcard: '%QUERY'
    }
});

const goToProductPage = function(item) {
    let url = '/products/' + item['_id'];
    window.location.assign(url);
};

const displayProductName = function (item) {
            return typeof item !== 'undefined' && typeof item.product_name != 'undefined' && item.product_name || item;
};

const basicTypeaheadConfig = {
    items: 'all',
    minLength: 1,
    matcher: function(x) { return true; },
    sorter: function(items) { return items; },
    autoSelect: false,
    source: bloodhound_names.ttAdapter(),
    displayText: displayProductName,
    afterSelect: goToProductPage
};

$('input.suggestions').typeahead(basicTypeaheadConfig);

// product page; TODO: remove from main file one it's ready

$('.add-ingredient').click(function(event) {
    var tmpl = document.querySelector('#ingredient_tmpl');
    var clone = document.importNode(tmpl.content, true);
    var tbody = this.closest('tbody');
    tbody.insertBefore(clone, this.closest('tr'));
    var searchList = tbody.querySelectorAll('input.prod-search');
    if (searchList.length) {
        var sbox = searchList[searchList.length - 1];
        sbox.focus();
        $(sbox).typeahead(basicTypeaheadConfig);

        $(sbox).change(function() {
            var cell = this.closest('td');
            cell.innerHTML = '<span class="glyphicon glyphicon-refresh spinning"></span> Loading...';
            // XXX: start product loading
        });
    }
});

$('.remove-ingredient').click(function(event) {
    var row = this.closest('tr');
    row.remove();
});
