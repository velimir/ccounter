defmodule CCounter.ProductView do
  use CCounter.Web, :view
  alias CCounter.Product

  def ingredients(%{"ingredients" => ingredients}) do
    for ingredient <- ingredients do
      case ingredient do
        %{"_id" => id} -> Product.find(%{"_id" => id})
        _              -> ingredient
      end
    end
  end

  def ingredient_name(%{"_id" => _id, "product_name" => name}), do: name
  def ingredient_name(%{"text" => name}), do: name

  def quantity(%{"quantity" => quantity}), do: quantity
  def quantity(_), do: ""

  def energy(%{"_id" => _id, "nutriments" => nutriments}, quantity) do
    case nutriments["energy_100g"] do
      nil        ->
        ""
      energy_num ->
        eng_per_100 = parse_float(energy_num, "")
        eng_per_100 / 100 * quantity
    end
  end
  def energy(_, _), do: ""

  def nutriments_headers(%{"nutriments" => nutriments}) do
    nutriments
    |> Enum.map(fn {name, _} -> split_name(name) end)
    |> Enum.filter(&match?([_item, _group], &1))
    |> Enum.map(fn [_, group] -> group end)
    |> Enum.into(MapSet.new)
  end

  def nutriments(%{"nutriments" => nutriments}) do
    nutriments
    |> group_by_item
    |> unify
  end

  def image_src(%{"images" => nil}), do: image_placeholder
  def image_src(%{"_id" => id, "images" => images}) do
    case off_image_name(images) do
      nil  -> image_placeholder
      name -> off_static_url(id, name)
    end
  end

  def next_page(conn, page) do
    pager_link(conn, page, %{page: page.page + 1})
  end

  def prev_page(conn, page) do
    pager_link(conn, page, %{page: page.page - 1})
  end

  defp pager_link(conn, page, overrides \\ %{}) do
    query = pager_query(page |> Map.merge(overrides))
    product_path(conn, :index, query)
  end

  defp pager_query(opts) do
    Enum.reduce opts, [], &process_query/2
  end

  defp process_query({_k, nil}, acc), do: acc
  defp process_query({:page = key, val}, acc), do: Keyword.put(acc, key, val)
  defp process_query({:search = key, val}, acc), do: Keyword.put(acc, key, val)
  defp process_query({_k, _v}, acc), do: acc

  # Private functions

  defp group_by_item(nutriments) do
    Enum.reduce(nutriments, %{}, fn({name, value}, acc) ->
      case split_name(name) do
        [item, group] -> update_item(acc, item, group, value)
        _ ->             acc
      end
    end)
  end

  defp split_name(name), do: String.split(name, ~r{_}, parts: 2)

  defp update_item(dict, item, group, value) do
    vals = Enum.into([{group, value}], %{})
    Dict.update(dict, item, vals, &Enum.into(vals, &1))
  end

  defp unify(nutriments, default \\ "?") do
    nutriments
    |> Enum.flat_map(fn {_key, group} -> Dict.keys(group) end)
    |> Enum.into(MapSet.new)
    |> Enum.reduce(nutriments, fn name, acc ->
      acc
      |> Enum.reduce(acc, fn {item, group}, acc ->
        group = Dict.put_new(group, name, default)
        Dict.put(acc, item, group)
      end)
    end)
  end

  defp image_placeholder(size \\ 200, text \\ "No image available") do
    %URI{
      scheme: "http",
      host: "placehold.it",
      path: "/#{size}",
      query: URI.encode_query([text: text])
    } |> URI.to_string
  end

  defp off_image_name(images, type \\ "front", size \\ 200) do
    case images[type]["rev"] do
      nil -> nil
      rev -> Enum.join([type, rev, size, "jpg"], ".")
    end
  end

  defp off_static_url(id, name) do
    %URI{
      scheme: "http",
      host: "static.openfoodfacts.org",
      path: "/images/products/#{ off_static_resource(id) }/#{ name }",
    } |> URI.to_string
  end

  defp off_static_resource(id) when byte_size(id) < 9, do: id
  defp off_static_resource(id) when byte_size(id) <= 12 do
    (for <<chunk :: binary-size(3) <- String.ljust(id, 12)>>, do: chunk)
    |> Enum.join("/")
    |> String.rstrip
  end
  defp off_static_resource(id), do: id |> split(3) |> Enum.join("/")

  defp split(string, length) do
    chunks = calc_chunks(byte_size(string), length)
    split_by_chunks(string, chunks)
  end

  defp calc_chunks(length, chunk_size) when div(length, chunk_size) >= 2 do
    [chunk_size | calc_chunks(length - chunk_size, chunk_size)]
  end
  defp calc_chunks(length, _chunk_size), do: [length]

  defp split_by_chunks(_string, []), do: []
  defp split_by_chunks(string, [len | chunks]) do
    # assuming that each codepoint is 1 byte length, which works
    # perfectly fine for digits (for which that function was designed)
    <<prefix :: binary-size(len), rest :: binary>> = string
    [prefix | split_by_chunks(rest, chunks)]
  end

  defp parse_float(value, default) do
    case Float.parse(value) do
      :error   -> default
      {num, _} -> num
    end
  end
end
