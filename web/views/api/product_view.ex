defmodule CCounter.Api.ProductView do
  use CCounter.Web, :view

  def render("index.json", %{products: products}) do
    render_many(products, CCounter.Api.ProductView, "product.json")
  end

  def render("show.json", %{product: product}) do
    render_one(product, CCounter.Api.ProductView, "product.json")
  end

  def render("product.json", %{product: product}), do: product
end
