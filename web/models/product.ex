defmodule CCounter.Product do
  # TODO: show only EN products
  use CCounter.Collection, name: "products", pool: CCounter.Repo

  def regex_find(name, override \\ %{}) do
    all(
      %{
        "product_name" => %{
          "$regex" => name,
          "$options" => "i"
        }
      })
    |> Map.merge(override)
  end

  def all(overrides \\ %{}) do
    %{"complete" => 1, "images" => %{"$ne" => nil}, "lc" => "en"}
    |> Map.merge(overrides)
  end
end
