defmodule CCounter.PageController do
  use CCounter.Web, :controller

  def index(conn, _params) do
    render conn, :index
  end
end
