defmodule CCounter.Api.ProductController do
  use CCounter.Web, :controller

  alias CCounter.Product
  require Logger

  def index(conn, %{"product_name" => name}) do
    products = Product.regex_find(name)
    |> Product.find(limit: 100)
    
    render(conn, "index.json", products: products)
  end
  def index(conn, _params) do
    products = Product.all
    |> Product.find(limit: 100)

    render(conn, "index.json", products: products)
  end

  def show(conn, %{"id" => id}) do
    [product] = Product.find(%{"_id" => id}) |> Enum.to_list
    render(conn, "show.json", product: product)
  end

end
