defmodule CCounter.ProductController do
  use CCounter.Web, :controller

  alias CCounter.Product
  require Logger

  plug :scrub_params, "product" when action in [:create, :update]

  def index(conn, params) do
    # as long as we have only 1 model and controller, which
    # should be paginated it's fine to leave all that mess here
    search = params["search"]
    query = index_query(search)
    per_page = 27
    total_count = Product.count(query)
    last_page = div(total_count, per_page) + 1
    page_num = page_number(params, last_page)
    entries = Product.find(
      query,
      sort: ["last_modified_t"],
      limit: per_page,
      skip: (page_num - 1) * per_page
    )

    render conn, :index, page: %{
      entries: entries,
      last_page: last_page,
      page: page_num,
      search: search
    }
    end

  defp index_query(nil), do: Product.all
  defp index_query(name), do: Product.regex_find(name)

  defp page_number(params, last_page, default \\ 1)
  defp page_number(%{"page" => page}, last_page, default) do
    case Integer.parse(page) do
      {num, _} when num < 0 or num > last_page -> default
      {num, _}                                 -> num
      _                                        -> default
    end
  end
  defp page_number(_params, _last_page, default), do: default

  def new(_conn, _params) do
    # changeset = Product.changeset(%Product{})
    # render(conn, "new.html", changeset: changeset)
  end

  def create(_conn, %{"product" => _product_params}) do
    # changeset = Product.changeset(%Product{}, product_params)

    # case Repo.insert(changeset) do
    #   {:ok, _product} ->
    #     conn
    #     |> put_flash(:info, "Product created successfully.")
    #     |> redirect(to: product_path(conn, :index))
    #   {:error, changeset} ->
    #     render(conn, "new.html", changeset: changeset)
    # end
  end

  def show(conn, %{"id" => id}) do
    [product] = Product.find(%{"_id" => id}) |> Enum.to_list
    render(conn, "show.html", product: product)
  end

  def edit(_conn, %{"id" => _id}) do
    # product = Repo.get!(Product, id)
    # changeset = Product.changeset(product)
    # render(conn, "edit.html", product: product, changeset: changeset)
  end

  def update(_conn, %{"id" => _id, "product" => _product_params}) do
    # product = Repo.get!(Product, id)
    # changeset = Product.changeset(product, product_params)

    # case Repo.update(changeset) do
    #   {:ok, product} ->
    #     conn
    #     |> put_flash(:info, "Product updated successfully.")
    #     |> redirect(to: product_path(conn, :show, product))
    #   {:error, changeset} ->
    #     render(conn, "edit.html", product: product, changeset: changeset)
    # end
  end

  def delete(_conn, %{"id" => _id}) do
    # product = Repo.get!(Product, id)

    # # Here we use delete! (with a bang) because we expect
    # # it to always work (and if it does not, it will raise).
    # Repo.delete!(product)

    # conn
    # |> put_flash(:info, "Product deleted successfully.")
    # |> redirect(to: product_path(conn, :index))
  end
end
