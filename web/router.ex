defmodule CCounter.Router do
  use CCounter.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CCounter do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    resources "/products", ProductController
  end

  scope "/api", CCounter.Api, as: :api do
    pipe_through :api

    get "/products", ProductController, :index
    get "/products/:id", ProductController, :show
  end
end
